﻿//
//  StaticPoints.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using ServiceStack.Data;
using ServiceStack.OrmLite;
using Hackathon.ServiceModel.Types;
using System.Linq;
using System;
using ServiceStack;
using System.Collections.Generic;

namespace Hackathon.ServiceInterface.Points
{
	public class StaticPoints : IPoints
	{
		public int GetPoints (Service service, int userId, int challengeId, DateTime solvedTime, DateTime accessDate, IEnumerable<int> accessedHintIds)
		{
			if (solvedTime != DateTime.MinValue) {
				// The challenge has been solved
				return 42;
			}
			//The challenge has not been solved, yet
			return 0;
		}

		public int GetPoints (Service service, int userId, int challengeId)
		{
			var challengeSolutions = service.Db.Select<ChallengeSolution> (c => c.UserId == userId && c.ChallengeId == challengeId);
			if (challengeSolutions.Any ()) {
				var solution = challengeSolutions.First ();
				return GetPoints (service, userId, challengeId, solution.SolutionTime, DateTime.MinValue, null);
			} else
				return GetPoints (service, userId, challengeId, DateTime.MinValue, DateTime.MinValue, null);
		}
	}
}

