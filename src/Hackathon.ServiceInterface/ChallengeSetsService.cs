﻿//
//  ChallengeSetsService.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Linq;
using ServiceStack;
using System.Collections.Generic;
using ServiceStack.OrmLite;
using Hackathon.ServiceModel;
using Hackathon.ServiceModel.Types;

namespace Hackathon.ServiceInterface
{
	public class ChallengeSetsService : Service
	{
		[Authenticate]
		public IList<int> Get (FindChallengeSets request)
		{
			IEnumerable<ChallengeSet> challengeSets = Db.Select<ChallengeSet> ();
			if (request.ChallengeId != null)
				challengeSets = challengeSets.Where (c => ((!c.Challenges.IsNullOrEmpty ()) && (c.Challenges.Contains (request.ChallengeId.Value))));
			return challengeSets.Select (c => c.Id).ToList ();
		}

		[Authenticate]
		public GetChallengeSetResponse Get (GetChallengeSet request)
		{
			var challengeSets = Db.Select<ChallengeSet> ().Where (c => (c.Id == request.Id));
			if (!challengeSets.Any ())
				throw HttpError.NotFound ("No such challengeset found");
			if (challengeSets.Count () > 1)
				throw HttpError.Conflict ("Internal database error");
			ChallengeSet challengeSet = challengeSets.First ();

			return challengeSet.ConvertTo<GetChallengeSetResponse> ();
		}
	}
}

