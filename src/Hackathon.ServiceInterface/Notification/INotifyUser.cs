﻿//
//  INotifyUser.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Hackathon.ServiceModel.Types;

namespace Hackathon.ServiceInterface.Notification
{
	/// <summary>
	/// Interface whose implementations can send some sort of notification to the user
	/// </summary>
	public interface INotifyUser
	{
		/// <summary>
		/// Notify the user with a message
		/// </summary>
		/// <param name="title">Title of the message (like an e-ail subject or a header line.</param>
		/// <param name="message">Message to be sent.</param>
		/// <param name="user">User who shall receive the message</param>
		void Notify (string title, string message, User user);
	}
}

