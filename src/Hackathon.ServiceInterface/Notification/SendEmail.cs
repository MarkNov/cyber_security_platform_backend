﻿//
//  SendEmail.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Hackathon.ServiceModel.Types;
using System.Net.Mail;

namespace Hackathon.ServiceInterface.Notification
{
	public class SendEmail : INotifyUser
	{
		public static readonly string smtpHost = "127.0.0.1";
		public static readonly int smtpPort = 25;

		public SendEmail ()
		{
		}

		public void Notify (string title, string message, User user)
		{
			if (user.MailAddress == null || user.MailAddress == "")
				throw new ArgumentException ("No e-mail address available for the user");
			
			MailMessage mailMsg = new MailMessage ();
			MailAddress to = new MailAddress (user.MailAddress, String.Format ("{0} {1}", user.FirstName, user.LastName));
			mailMsg.To.Add (to);

			MailAddress from = new MailAddress ("noreply@fh-campuswien.ac.at");
			mailMsg.From = from;

			// Subject and Body
			mailMsg.Subject = title;
			mailMsg.Body = message;

			SmtpClient smtpClient = new SmtpClient (smtpHost, smtpPort);
			//			System.Net.NetworkCredential credentials = 
			//				new System.Net.NetworkCredential("username", "password");
			//			smtpClient.Credentials = credentials;

			smtpClient.Send (mailMsg);
		}
	}
}

