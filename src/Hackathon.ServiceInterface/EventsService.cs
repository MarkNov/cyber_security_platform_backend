﻿//
//  EventsService.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>, ${AuthorCompany}
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Linq;
using ServiceStack;
using System.Collections.Generic;
using ServiceStack.OrmLite;
using Hackathon.ServiceModel;
using Hackathon.ServiceModel.Types;

namespace Hackathon.ServiceInterface
{
	public class EventsService : Service
	{
		/// <summary>
		/// Fetches an event from the database.
		/// Throws appropriate HttpError Exceptions in case of an error
		/// </summary>
		/// <returns>The desiredevent.</returns>
		/// <param name="id">Identifier of the event.</param>
		private Event GetEventById (int id)
		{
			var Events = Db.Select<Event> ().Where (c => (c.Id == id));
			if (!Events.Any ())
				throw HttpError.NotFound ("No such Event found");
			if (Events.Count () > 1)
				throw HttpError.Conflict ("Internal database error");
			return Events.First ();
		}

		[Authenticate]
		public IList<int> Get (FindEvents request)
		{
			if (request.ChallengeSetId != null)
				return Db.Select<EventChallengeSets> (e => e.ChallengeSetId == request.ChallengeSetId.Value).Select (e => e.EventId).ToList ();
			else
				return Db.Select<Event> ().Select (c => c.Id).ToList ();
		}

		[Authenticate]
		public GetEventResponse Get (GetEvent request)
		{
			Event event_ = GetEventById (request.Id);
			var challengeSetsForEvent = Db.Select<EventChallengeSets> ().Where (e => e.EventId == event_.Id);
			//Filter out the ChallengeSets which are currently available, if desired
			if (request.OnlyCurrent)
				challengeSetsForEvent = challengeSetsForEvent.Where (c => ((DateTime.Compare (c.StartDate, DateTime.Now) < 0) && (DateTime.Compare (c.EndDate, DateTime.Now) > 0)));
			else if (request.OnlyCurrentAndPast)
				challengeSetsForEvent = challengeSetsForEvent.Where (c => ((DateTime.Compare (c.StartDate, DateTime.Now) < 0)));
			
			GetEventResponse ret = event_.ConvertTo<GetEventResponse> ();
			ret.ChallengeSets = challengeSetsForEvent.Select (e => e.ToDto ()).ToList ();
			return ret;
		}

		[Authenticate]
		public IList<int> Get (GetEventUsers request)
		{
			//We do not acutally need the event, but fetch it anyway to validate its existence (and cause an error otherwise)
			GetEventById (request.Id);

			var users = Db.Select<EventUsers> ().Where (e => e.EventId == request.Id).Select (e => e.UserId).ToList ();
			// We must only return the users of this event, if the authenticated user is part of it
			int loggedInUserId = Int32.Parse (GetSession ().UserAuthId);
			if (users.Contains (loggedInUserId))
				return users;
			else
				throw HttpError.Forbidden ("Your are not part of this event.");
		}

		[Authenticate]
		public IList<EventRankingResponse> Get (GetEventRanking request)
		{
			var eventUsers = Get (new GetEventUsers (){ Id = request.Id });
			var challengeSetIds = Get (new GetEvent (){ Id = request.Id }).ChallengeSets.Select (c => c.ChallengeSetId);

			//Build a list of all challenges in this event
			List<int> challengeIds = new List<int> ();
			using (var challengeSetsService = ResolveService<ChallengeSetsService> ()) {
				foreach (int challengeSetId in challengeSetIds)
					challengeIds.AddRange (challengeSetsService.Get (new GetChallengeSet (){ Id = challengeSetId }).Challenges);
			}
			challengeIds = challengeIds.Distinct ().ToList ();

			//Query to select the points from the database
			var query = Db.From<User> ()
				.LeftJoin<User, ChallengeSolution> ((u, s) => u.Id == s.UserId && Sql.In (s.ChallengeId, challengeIds))
				.Where<User> (u => Sql.In (u.Id, eventUsers))
				.GroupBy (u => u.Id) //Select the sum of points
				.Select<User, ChallengeSolution> ((u, s) => new {UserId = Sql.As (u.Id, "UserId"), u.UserName, Points = Sql.Sum (s.Points)});

			List<EventRankingResponse> ret = Db.SqlList<EventRankingResponse> (query).OrderByDescending (r => r.Points).ToList ();

			// Set the rank
			for (int i = 1; i <= ret.Count (); ++i)
				ret [i - 1].Rank = i;

			return ret;
		}
	}
}

