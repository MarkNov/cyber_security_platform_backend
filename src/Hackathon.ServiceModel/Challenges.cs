﻿//
//  Challenges.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ServiceStack;
using System.Collections.Generic;
using Hackathon.ServiceModel.Types;

namespace Hackathon.ServiceModel
{
	[Route ("/challenges/{id}", "GET")]
	public class FindChallenges : IReturn<FindChallengeResponse>
	{
		public int Id { get; set; }
	}

	public class FindChallengeResponse
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public int BasePoints { get; set; }
	}

	[Route ("/challenges/{id}/description", "GET")]
	public class GetChallengeDescription : IReturn<GetChallengeDescriptionResponse>
	{
		public int Id { get; set; }
	}

	[Route ("/challenges/{id}/solution", "POST")]
	public class PostSolution : IReturnVoid
	{
		public int Id { get; set; }

		public string Solution { get; set; }
	}

	[Route ("/challenges/{id}/hints", "GET")]
	public class GetChallengeHints : IReturn<List<int>>
	{
		public int Id { get; set; }
	}

	[Route ("/challenges/{id}/hints/{hintnumber}", "GET")]
	public class GetChallengeHint : IReturn<GetChallengeHintResponse>
	{
		public int Id { get; set; }

		public int HintNumber { get; set; }
	}

	[Route ("/challenges/{id}/hints/{hintnumber}/pointreduction", "GET")]
	public class GetHintPointReduction : IReturn<PointResponse>
	{
		public int Id { get; set; }

		public int HintNumber { get; set; }
	}

	public class PointResponse
	{
		public int Points { get; set; }
	}

	public class GetChallengeDescriptionResponse
	{
		public string Description { get; set; }
	}

	public class GetChallengeHintResponse
	{
		public int HintNumber { get; set; }

		public string Text { get; set; }
	}
}

