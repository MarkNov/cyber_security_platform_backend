﻿//
//  ChallengeAccess.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ServiceStack.DataAnnotations;

namespace Hackathon.ServiceModel.Types
{
	public enum AccessType
	{
		Description = 1,
		Attachments
	}

	[CompositeKey ("UserId", "ChallengeId", "Type")]
	public class ChallengeAccess
	{
		[AutoIncrement]
		public int Id { get; set; }

		[References (typeof(User))]
		public int UserId { get; set; }

		[References (typeof(Challenge))]
		public int ChallengeId { get; set; }

		public AccessType Type { get; set; }

		public DateTime AccessTime { get; set; }
	}
}
