﻿//
//  ConvertExtensions.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ServiceStack;
using Hackathon.ServiceModel.Types;
using System.Globalization;

namespace Hackathon.ServiceModel
{
	public static class ConvertExtensions
	{
		/// <summary>
		/// Convert a DateTime into a string format defined by ECMAScript according to https://es5.github.io/#x15.9.1.15
		/// </summary>
		/// <returns>The ECMA script string.</returns>
		/// <param name="date">Date.</param>
		public static String ToEcmaScriptString (this DateTime date)
		{
			return date.ToString ("yyyy-MM-ddTHH:mm:ss.fffK");
		}

		public static DateTime FromEcmaScriptString (this string date)
		{
			return DateTime.ParseExact (date, "yyyy-MM-ddTHH:mm:ss.fffK", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
		}


		public static ChallengeSetInEvent ToDto (this EventChallengeSets from)
		{
			var to = from.ConvertTo<ChallengeSetInEvent> ();
			to.StartDate = from.StartDate.ToLocalTime ().ToEcmaScriptString ();
			to.EndDate = from.EndDate.ToLocalTime ().ToEcmaScriptString ();
			return to;
		}
	}
}

