﻿//
//  Events.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ServiceStack;
using System.Collections.Generic;

namespace Hackathon.ServiceModel
{
	[Route ("/events/", "GET")]
	public class FindEvents : IReturn<IList<int>>
	{
		public int? ChallengeSetId { get; set; }
	}

	[Route ("/events/{id}", "GET")]
	public class GetEvent : IReturn<GetEventResponse>
	{
		public int Id { get; set; }

		public bool OnlyCurrent { get; set; }

		public bool OnlyCurrentAndPast { get; set; }
	}

	[Route ("/events/{id}/users", "GET")]
	public class GetEventUsers : IReturn<IList<int>>
	{
		public int Id { get; set; }
	}

	public class ChallengeSetInEvent
	{
		public int ChallengeSetId { get; set; }

		public String StartDate { get; set; }

		public String EndDate { get; set; }

		public override bool Equals (object obj)
		{
			if (obj == null)
				return false;
			if (ReferenceEquals (this, obj))
				return true;
			if (obj.GetType () != typeof(ChallengeSetInEvent))
				return false;
			ChallengeSetInEvent other = (ChallengeSetInEvent)obj;
			return ChallengeSetId == other.ChallengeSetId && StartDate == other.StartDate && EndDate == other.EndDate;
		}

		public override int GetHashCode ()
		{
			unchecked {
				return ChallengeSetId.GetHashCode () ^ StartDate.GetHashCode () ^ EndDate.GetHashCode ();
			}
		}
	}

	public class GetEventResponse
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public string Description { get; set; }

		public List<ChallengeSetInEvent> ChallengeSets { get; set; }
	}

	[Route ("/events/{id}/ranking", "GET")]
	public class GetEventRanking : IReturn<IList<EventRankingResponse>>
	{
		public int Id { get; set; }
	}

	public class EventRankingResponse
	{
		public int UserId { get; set; }

		public string UserName { get; set; }

		public int Rank { get; set; }

		public int Points { get; set; }
	}
}

