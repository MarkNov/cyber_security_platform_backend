﻿//
//  DbAuthProvider.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Web;
using System.Collections.Generic;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using Hackathon.ServiceModel.Types;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Hackathon.ServiceModel.Auth
{
	public class DbAuthProvider: CredentialsAuthProvider
	{
		public static string HashPassword (string salt, string password)
		{
			using (var hash = SHA256.Create ()) {
				string hashedPassword = Convert.ToBase64String (
					                        hash.ComputeHash (UTF8Encoding.UTF8.GetBytes (salt).Concat (UTF8Encoding.UTF8.GetBytes (password)).ToArray ())
				                        );
				return hashedPassword;
			}

		}

		public override bool TryAuthenticate (IServiceBase authService, string userName, string password)
		{
			//Check the input variables
			if (userName.IsNullOrEmpty () || password.IsNullOrEmpty ())
				return false;
			
			//Return true if credentials are valid, otherwise false
			using (var db = authService.TryResolve<IDbConnectionFactory> ().Open ()) {
				var users = db.Select<User> (u => ((u.MailAddress == userName) || (u.UserName == userName)));
				if (users.Count == 1) {
					User user = users.First ();
					string hashedPassword = HashPassword (user.Salt, password);
					return (hashedPassword == user.Password);
				}
			}
			// If we reach this point, the authentication has failed
			return false;
		}

		public override IHttpResult OnAuthenticated (IServiceBase authService, IAuthSession session, IAuthTokens tokens, 
		                                             Dictionary<string, string> authInfo)
		{
			//Fill IAuthSession with data you want to retrieve in the app eg:
			//session.FirstName = "some_firstname_from_db";
			//...
			using (var db = authService.TryResolve<IDbConnectionFactory> ().Open ()) {
				User user = db.Select<User> ().Where (u => (u.MailAddress == session.UserAuthName) || (u.UserName == session.UserAuthName)).FirstOrDefault ();
				if (user == null)
					throw HttpError.Conflict ("User not found");
				session.UserAuthName = user.UserName;
				session.UserName = user.UserName;

				session.UserAuthId = user.Id.ToString ();
				session.DisplayName = null;
			}
			//Call base method to Save Session and fire Auth/Session callbacks:
			var res = base.OnAuthenticated (authService, session, tokens, authInfo);
			return res;

			//Alternatively avoid built-in behavior and explicitly save session with
			//authService.SaveSession(session, SessionExpiry);
			//return null;
		}
	}
}

