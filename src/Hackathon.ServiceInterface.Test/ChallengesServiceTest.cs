﻿//
//  ChallengesServiceTest.cs
//
//  Author:
//       Mathias Tausig <mathias.tausig@fh-campuswien.ac.at>
//
//  Copyright (c) 2016 University of Applied Sciences Campus Vienna
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using NUnit.Framework;
using System;
using System.Linq;
using ServiceStack.Data;
using Hackathon.ServiceModel.Types;
using ServiceStack;
using Hackathon.ServiceModel;
using ServiceStack.Testing;
using ServiceStack.OrmLite;
using ServiceStack.Auth;
using System.Collections.Generic;
using System.Net;
using Hackathon.ServiceInterface.Points;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

namespace Hackathon.ServiceInterface.Test
{
	[TestFixture ()]
	public class ChallengesServiceTest
	{
		private class TestAppHost : BasicAppHost
		{
			public TestAppHost () : base (typeof(ChallengesService).Assembly)
			{
			}

			public override void Configure (Funq.Container container)
			{
				container.Register<IDbConnectionFactory> (c =>
					new OrmLiteConnectionFactory (":memory:", SqliteDialect.Provider)
				);

				container.Register<IAuthSession> (c => new AuthUserSession {
					UserAuthName = "foobar@mailinator.com",
					UserName = "foobar",
					UserAuthId = "1",
				});
				
				container.RegisterAs<DbPoints, IPoints> ();

				byte[] userTokenKey = new byte[]{ 0x31, 0x32, 0x33 };
				HMACSHA256 userTokenCalculator = new HMACSHA256 (userTokenKey);
				container.Register<HMAC> (userTokenCalculator);

				using (var db = container.TryResolve<IDbConnectionFactory> ().Open ()) {
					db.DropAndCreateTable<Challenge> ();
					db.DropAndCreateTable<ChallengeSet> ();
					db.DropAndCreateTable<Event> ();
					db.DropAndCreateTable<EventUsers> ();
					db.DropAndCreateTable<EventChallengeSets> ();
					db.DropAndCreateTable<Hint> ();
					db.DropAndCreateTable<User> ();
					db.DropAndCreateTable<ChallengeAccess> ();
					db.DropAndCreateTable<HintAccess> ();
					db.DropAndCreateTable<ChallengeSolution> ();


					db.Insert (new Challenge {
						Id = 1,
						Title = "foobar",
						Description = "Foo bar foo bar.",
						Solution = "This is it!",
						BasePoints = 100,
					});

					db.Insert (new Challenge {
						Id = 2,
						Title = "Another challenge",
					});

					db.Insert (new ChallengeSet {
						Id = 1,
						Challenges = new List<int> { 1 }
					});

					db.Insert (new Event {
						Id = 1
					});

					db.Insert (new EventChallengeSets {
						ChallengeSetId = 1,
						EventId = 1,
						StartDate = DateTime.Now,
						EndDate = DateTime.Now.Add (new TimeSpan (1, 0, 0)),
					});

					db.Insert (new EventUsers {
						EventId = 1,
						UserId = 1,
					});

					db.Insert (new Hint {
						Id = 1,
						ChallengeId = 2,
						Text = "Hint for challenge 2"
					});

					db.Insert (new Hint {
						Id = 2,
						ChallengeId = 1,
						Text = "Hint #1"
					});

					db.Insert (new Hint {
						Id = 3,
						ChallengeId = 1,
						Text = "The second hint"
					});

					db.Insert (new User {
						Id = 1,
						UserName = "foobar",
						FirstName = "Foo",
						LastName = "bar",
						Avatar = null,
						Salt = "foobar",
						MailAddress = "foobar@mailinator.com",
						//pw=123456, created with echo -n "foobar123456"|sha256sum, then base64 encoded
						Password = @"V7yttgjbKKO8rKp6OTiHqQ8BCSEKqjaGgBtun4lWjfs=",
					});

				}
			}
		}

		private ServiceStackHost appHost;

		[SetUp]
		public void SetupChallengesServiceTest ()
		{
			try {
				appHost = new TestAppHost ();
				appHost.Init ();
			} catch (Exception e) {
				Assert.Fail ("Apphost startup failed: {0}", e.ToString ());
			}
		}

		//[TestFixtureTearDown]
		[TearDown]
		public void TestFixtureTearDown ()
		{
			appHost.Dispose ();
		}


		[Test ()]
		public void GetExistingChallengeTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				var challenge = service.Get (new FindChallenges (){ Id = 1 });

				Assert.That (challenge.Title, Is.EqualTo ("foobar"));
				Assert.That (challenge.Id, Is.EqualTo (1));
				Assert.That (challenge.BasePoints, Is.EqualTo (100));
			}
		}

		[Test ()]
		public void GetExistingChallengeNotYetActiveTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				service.Db.Insert<Event> (new Event{ Id = 2 });
				service.Db.Insert<Challenge> (new Challenge{ Id = 3 });
				service.Db.Insert<ChallengeSet> (new ChallengeSet {
					Id = 2,
					Challenges = new List<int>{ 3 }
				});
				service.Db.Insert<EventChallengeSets> (new EventChallengeSets {
					EventId = 2, 
					ChallengeSetId = 2,
					StartDate = DateTime.Now.AddDays (1),
					EndDate = DateTime.Now.AddDays (2)
				});
				service.Db.Insert<EventUsers> (new EventUsers{ EventId = 2, UserId = 1 });


				try {
					service.Get (new FindChallenges (){ Id = 3 });
					Assert.Fail ("Retrieving a challenge which is not active yet should fail.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void GetExistingChallengeExpiredTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				service.Db.Insert<Event> (new Event{ Id = 2 });
				service.Db.Insert<Challenge> (new Challenge{ Id = 3 });
				service.Db.Insert<ChallengeSet> (new ChallengeSet {
					Id = 2,
					Challenges = new List<int>{ 3 }
				});
				service.Db.Insert<EventChallengeSets> (new EventChallengeSets {
					EventId = 2,
					ChallengeSetId = 2,
					StartDate = DateTime.Now.Subtract (new TimeSpan (2, 0, 0, 0)),
					EndDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0, 0)),
				});
				service.Db.Insert<EventUsers> (new EventUsers{ EventId = 2, UserId = 1 });

				//Retrieving this challenge should work without a problem
				service.Get (new FindChallenges (){ Id = 3 });
			}
		}

		[Test ()]
		public void GetExistingChallengeNotInEventOfUser ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new FindChallenges (){ Id = 2 });
					Assert.Fail ("Accessing a challenge no availabe in a user event should not be allowed.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void GetChallengeDescriptionNotInEventOfUser ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetChallengeDescription (){ Id = 2 });
					Assert.Fail ("Accessing a challenge description no availabe in a user event should not be allowed.");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void GetNonExistingChallengeTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new FindChallenges (){ Id = 666 });
					Assert.Fail ("Should throw on lookup up non existing ID");
				} catch (HttpError e) {
					// The return code will be Forbidden, since an inexisting event is definitely not part of a user's event
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void GetChallengeDescriptionTest ()
		{
			int challengeId = 1;
			int userId = 1; //from the mocked session definition in the apphost's configure methode
			using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
				Assert.That (db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == userId && a.Type == AccessType.Description)).Any (), Is.False);
				using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
					string description = service.Get (new GetChallengeDescription (){ Id = challengeId }).Description;
					Assert.That (description, Is.EqualTo ("Foo bar foo bar."));
					//Now check if the access time has been set
					DateTime accessTime = db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == userId && a.Type == AccessType.Description)).First ().AccessTime;
					Assert.That (DateTime.Now.Subtract (accessTime).TotalSeconds, Is.LessThan (1));
					//Check that the access time does not change on another request
					service.Get (new GetChallengeDescription (){ Id = challengeId });
					DateTime secondAccessTime = db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == userId && a.Type == AccessType.Description)).First ().AccessTime;
					Assert.That (accessTime, Is.EqualTo (secondAccessTime));
				}
			}
		}

		[Test ()]
		public void GetChallengeDescriptionWithReplacementTest ()
		{
			int challengeId = 1;
			using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
				db.UpdateNonDefaults<Challenge> (new Challenge (){ Description = "foo {{{UserToken-abcde}}} bar" }, c => c.Id == challengeId);
				using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
					string description = service.Get (new GetChallengeDescription (){ Id = challengeId }).Description;
					Match findToken = Regex.Match (description, @"^foo ([0-9a-zA-Z]+) bar$");
					Assert.That (findToken.Success);
					// HMAC Key (defined in the apphost ist {0x31, 0x32, 0x33}, HMAC-SHA256 should be calculated over token||username -> "abcdefoobar"
					// echo -n "abcdefoobar" |openssl dgst -mac hmac -sha256 -macopt key:123 -binary|base64
					//     XfReLAdjo/6ZT3jIPakMl/JqFHRo2yqFr0kpb86MEHM=
					//remove special characters from the string
					Assert.That (findToken.Groups [1].Value, Is.EqualTo ("XfReLAdjo6ZT3jIPakMlJqFHRo2yqFr0kpb86MEHM"));
				}
			}
		}

		[Test ()]
		public void GetChallengeDescriptionNonExistingTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetChallengeDescription (){ Id = 666 });
					Assert.Fail ("Should throw on lookup up non existing ID");
				} catch (HttpError e) {
					// The return code will be Forbidden, since an inexisting event is definitely not part of a user's event
					Assert.That (e.StatusCode, Is.EqualTo (HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void GetChallengeDescriptionExpiredTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				service.Db.Insert<Event> (new Event{ Id = 2 });
				int challengeId = (int)service.Db.Insert<Challenge> (new Challenge (){ Description = "The Description" }, selectIdentity: true);
				service.Db.Insert<ChallengeSet> (new ChallengeSet {
					Id = 2,
					Challenges = new List<int>{ challengeId }
				});
				service.Db.Insert<EventChallengeSets> (new EventChallengeSets {
					EventId = 2,
					ChallengeSetId = 2,
					StartDate = DateTime.Now.Subtract (new TimeSpan (2, 0, 0, 0)),
					EndDate = DateTime.Now.Subtract (new TimeSpan (1, 0, 0, 0)),
				});
				service.Db.Insert<EventUsers> (new EventUsers{ EventId = 2, UserId = 1 });

				//Retrieving this challenge's description should work without a problem
				service.Get (new GetChallengeDescription (){ Id = challengeId });
				//Check if the access time has been set
				DateTime accessTime = service.Db.Select<ChallengeAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == 1 && a.Type == AccessType.Description)).First ().AccessTime;
				Assert.That (DateTime.Now.Subtract (accessTime).TotalSeconds, Is.LessThan (1));
			}
		}

		[Test ()]
		public void GetChallengeHintsTest ()
		{
			int challengeId = 1;
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				var hints = service.Get (new GetChallengeHints (){ Id = challengeId });
				Assert.That (hints.Count (), Is.EqualTo (2));
			}
		}

		[Test ()]
		public void GetChallengeHintsNonExistingChallengeTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetChallengeHints (){ Id = 666 });
					Assert.Fail ("Should throw on lookup up non existing ID");
				} catch (HttpError e) {
					// Return code is Forbidden, beacause a non-existing challenge is no accesible by the user
					Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.Forbidden));
				}
			}
		}

		[Test ()]
		public void GetChallengeHintTest ()
		{
			int challengeId = 1;
			int userId = 1; //from the mocked session definition in the apphost's configure methode
			using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
				using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
					int hintId = service.Get (new GetChallengeHints{ Id = challengeId }).Last (); //test the second hint
					Assert.That (db.Select<HintAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == userId && a.HintId == hintId)).Any (), Is.False);
					string hint = service.Get (new GetChallengeHint () {
						Id = challengeId,
						HintNumber = hintId
					}).Text;
					Assert.That (hint, Is.EqualTo ("The second hint"));
					//Now check if the access time has been set
					DateTime accessTime = db.Select<HintAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == userId && a.HintId == hintId)).First ().AccessTime;
					Assert.That (DateTime.Now.Subtract (accessTime).TotalSeconds, Is.LessThan (1));
					//Check that the access time does not change on another request
					service.Get (new GetChallengeHint () {
						Id = challengeId,
						HintNumber = hintId
					});
					DateTime secondAccessTime = db.Select<HintAccess> ().Where (a => (a.ChallengeId == challengeId && a.UserId == userId && a.HintId == hintId)).First ().AccessTime;
					Assert.That (accessTime, Is.EqualTo (secondAccessTime));
				}
			}
		}

		[Test ()]
		public void GetChallengeHintsNonExistingHintTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				try {
					service.Get (new GetChallengeHint (){ Id = 1, HintNumber = 666 });
					Assert.Fail ("Should throw on lookup up non existing ID");
				} catch (HttpError e) {
					Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.NotFound));
				}
			}
		}

		[Test ()]
		public void PostSolutionTest ()
		{
			string solution = "This is it!";
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				//store out first access to the challenge in the db
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					ChallengeId = 1,
					UserId = 1,
					Type = AccessType.Description,
					AccessTime = DateTime.Now
				});
				service.Post (new PostSolution { Id = 1, Solution = solution });
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					var solutionInDb = db.Select<ChallengeSolution> (c => (c.ChallengeId == 1 && c.UserId == 1)).First ();
					Assert.That (solutionInDb.Solved);
					Assert.That (DateTime.Now.Subtract (solutionInDb.SolutionTime).TotalSeconds, Is.LessThan (1));
					Assert.That (solutionInDb.Points.Value, Is.GreaterThan (0), "A positive point value should be stored in the database after posting a correct solution");
				}
			}
		}

		[Test ()]
		public void PostBadSolutionsTest ()
		{
			string solution = "This is it!";
			string badSolution = "foobar";

			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				//store out first access to the challenge in the db
				service.Db.Insert<ChallengeAccess> (new ChallengeAccess {
					ChallengeId = 1,
					UserId = 1,
					Type = AccessType.Description,
					AccessTime = DateTime.Now
				});

				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					try {
						service.Post (new PostSolution { Id = 1, Solution = badSolution });
						Assert.Fail ("Posting a bad solution should result in an error.");
					} catch (HttpError e) {
						Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.BadRequest));
					}
					var solutionInDb = db.Select<ChallengeSolution> (c => (c.ChallengeId == 1 && c.UserId == 1)).First ();
					Assert.That (!solutionInDb.Solved);
					Assert.That (solutionInDb.WrongEntries, Is.EqualTo (1));
					Assert.That (solutionInDb.Points, Is.Null, "No points should be stored in the database after posting a bad solution");
					//post the bad solution again
					try {
						service.Post (new PostSolution { Id = 1, Solution = badSolution });
						Assert.Fail ("Posting a bad solution should result in an error.");
					} catch (HttpError e) {
						Assert.That (e.StatusCode, Is.EqualTo (System.Net.HttpStatusCode.BadRequest));
					}
					solutionInDb = db.Select<ChallengeSolution> (c => (c.ChallengeId == 1 && c.UserId == 1)).First ();
					Assert.That (!solutionInDb.Solved);
					Assert.That (solutionInDb.WrongEntries, Is.EqualTo (2));
					//now post the correct solution
					service.Post (new PostSolution { Id = 1, Solution = solution });
					solutionInDb = db.Select<ChallengeSolution> (c => (c.ChallengeId == 1 && c.UserId == 1)).First ();
					Assert.That (solutionInDb.Solved);
					Assert.That (solutionInDb.WrongEntries, Is.EqualTo (2));
					Assert.That (DateTime.Now.Subtract (solutionInDb.SolutionTime).TotalSeconds, Is.LessThan (1));
					Assert.That (solutionInDb.Points.Value, Is.GreaterThan (0), "A positive point value should be stored in the database after posting a correct solution");
				}
			}
		}

		[Test ()]
		public void GetHintPointReductionTest ()
		{
			using (var service = new ChallengesService { Request = new MockHttpRequest () }) {
				using (var db = appHost.TryResolve<IDbConnectionFactory> ().Open ()) {
					//prepare the database, such that the challenge is solved without a speed bonus
					DateTime startDate = DateTime.Now.ToLocalTime ();
					db.UpdateNonDefaults (new EventChallengeSets {
						StartDate = startDate,
						EndDate = startDate.AddDays (10),
					}, c => c.EventId == 1 && c.ChallengeSetId == 1);

					db.Insert (new ChallengeAccess () {
						AccessTime = startDate.AddSeconds (1),
						ChallengeId = 1,
						Type = AccessType.Description,
						UserId = 1
					});

					db.Insert (new Hint () { Id = 4, ChallengeId = 1, PointReduction = 0 });
					db.Insert (new Hint () { Id = 5, ChallengeId = 1, PointReduction = 30 });

					int pointReduction = service.Get (new GetHintPointReduction {
						Id = 1,
						HintNumber = 4
					}).Points;
					// We should loose the full speed bonus (40%) but nothing further
					Assert.That (pointReduction, Is.EqualTo (40));

					pointReduction = service.Get (new GetHintPointReduction {
						Id = 1,
						HintNumber = 5
					}).Points;
					// We should loose the full speed bonus (40%) plus 30% for the hint
					Assert.That (pointReduction, Is.EqualTo (70));

					db.Insert (new HintAccess () {
						HintId = 4,
						ChallengeId = 1,
						AccessTime = startDate.AddSeconds (2),
						UserId = 1,
					});
					pointReduction = service.Get (new GetHintPointReduction {
						Id = 1,
						HintNumber = 5
					}).Points;
					// Speed bonus is already gone because of the used hint #1, we should loose just the 30% of hint 2 now
					Assert.That (pointReduction, Is.EqualTo (30));

				}
			}
		}
	}
}

